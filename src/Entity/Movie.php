<?php

namespace App\Entity;

use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\NotNull;

/**
 * @ORM\Entity()
 */
class Movie
{
    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="movies")
     * @ORM\JoinColumn(nullable=false)
     */
    private $addedBy;

    public function __construct(
        /**
         * @ORM\Id
         * @ORM\GeneratedValue
         * @ORM\Column(type="integer")
         */
        private ?int $id = null,

        /**
         * @ORM\Column(type="string", length=255)
         */
        #[NotNull]
        private ?string $title = null,

        /**
         * @ORM\Column(type="date")
         */
        #[NotNull]
        private ?DateTimeInterface $releaseDate = null,

        /**
         * @ORM\Column(type="string", length=255)
         */
        #[NotNull]
        private string $director = '',
    ) {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setDirector(?string $director): void
    {
        $this->director = $director;
    }

    public function getDirector(): ?string
    {
        return $this->director;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getReleaseDate(): ?DateTimeInterface
    {
        return $this->releaseDate;
    }

    public function setReleaseDate(DateTimeInterface $releaseDate): void
    {
        $this->releaseDate = $releaseDate;
    }

    public function getHash(): string
    {
        return md5($this->title);
    }

    public function getAddedBy(): ?User
    {
        return $this->addedBy;
    }

    public function setAddedBy(?User $addedBy): self
    {
        $this->addedBy = $addedBy;

        return $this;
    }
}
