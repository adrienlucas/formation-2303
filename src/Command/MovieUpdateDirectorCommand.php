<?php

namespace App\Command;

use App\Entity\Movie;
use App\Gateway\OmdbGateway;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class MovieUpdateDirectorCommand extends Command
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private OmdbGateway $omdbGateway,
    ) {
        parent::__construct();
    }

    protected static $defaultName = 'app:movie:update-director';
    protected static $defaultDescription = 'Update movies directors.';

    protected function configure()
    {
        $this
            ->setDescription(self::$defaultDescription)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $directorsUpdated = 0;
        /** @var array<Movie> $movies */
        $movies = $this->entityManager->getRepository(Movie::class)
//            ->findBy(['director' => '']);
            ->findByDirector('');

        foreach ($movies as $movie) {
            $director = $this->omdbGateway->getDirectorByMovie($movie);
            $movie->setDirector($director);
            ++$directorsUpdated;
        }

        $this->entityManager->flush();

        $io->success(sprintf(
            'The directors have been updated (%d).',
            $directorsUpdated
        ));

        return Command::SUCCESS;
    }
}
