<?php

namespace App\Security;

use Exception;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class AuthenticationUtilsDecorator extends AuthenticationUtils
{
    public function __construct(
        private AuthenticationUtils $actualUtils,
    ) {
    }

    public function getLastAuthenticationError(bool $clearSession = true)
    {
        $error = $this->actualUtils->getLastAuthenticationError();
        if ($error === null) {
            return $error;
        }

        return new Exception('Bad username/password couple.');
    }

    public function getLastUsername()
    {
        return $this->actualUtils->getLastUsername();
    }
}
