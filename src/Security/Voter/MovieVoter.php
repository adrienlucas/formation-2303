<?php

namespace App\Security\Voter;

use App\Entity\Movie;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class MovieVoter extends Voter
{
    protected function supports($attribute, $subject)
    {
        return in_array($attribute, ['view'])
            && $subject instanceof \App\Entity\Movie;
    }

    /**
     * @param Movie $subject
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        if (
            !$user instanceof UserInterface
            || $attribute !== 'view'
        ) {
            return false;
        }

        return $subject->getAddedBy()->getId() === $user->getId();
    }
}
