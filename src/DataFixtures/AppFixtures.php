<?php

namespace App\DataFixtures;

use App\Entity\Movie;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $adrien = new User();
        $adrien->setUsername('adrien');
        $adrien->setRoles(['ROLE_ADMIN']);
        $adrien->setPassword('$argon2id$v=19$m=65536,t=4,p=1$8RzCB7kfcVcK1RWy/2WVjw$+W5zbq9p9jtrEEuf73MTHbsove+u2ECWH1OJAaKz2/8');

        $erwan = new User();
        $erwan->setUsername('erwan');
        $erwan->setRoles(['ROLE_USER']);
        $erwan->setPassword('$argon2id$v=19$m=65536,t=4,p=1$INig5YxZJb/wZ4vHshablA$RWvhtXYaWwe8SukXPMtZKf9Zufqxbn5uUSZuL/fg3RY');

        $manager->persist($adrien);
        $manager->persist($erwan);

        $movie = new Movie();
        $movie->setTitle('Alice in wonderland');
        $movie->setReleaseDate(new \DateTime('2010-2-25'));
        $movie->setAddedBy($adrien);
        $manager->persist($movie);

        $movie = new Movie();
        $movie->setTitle('Taxi');
        $movie->setReleaseDate(new \DateTime('1998-4-8'));
        $movie->setAddedBy($adrien);
        $manager->persist($movie);

        $movie = new Movie();
        $movie->setTitle('The little mermaid');
        $movie->setReleaseDate(new \DateTime('1989-11-17'));
        $movie->setAddedBy($erwan);
        $manager->persist($movie);

        $manager->flush();
    }
}
