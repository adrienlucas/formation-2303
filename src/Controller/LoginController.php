<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class LoginController extends AbstractController
{
    #[Route('/login', name: 'login')]
    public function index(AuthenticationUtils $utils): Response
    {
        return $this->render('login/index.html.twig', [
            'last_username' => $utils->getLastUsername(),
            'last_auth_error' => $utils->getLastAuthenticationError(),
        ]);
    }
}
