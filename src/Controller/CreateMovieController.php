<?php

namespace App\Controller;

use App\Entity\Movie;
use App\Form\MovieType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("ROLE_ADMIN")
 */
class CreateMovieController extends AbstractController
{
    #[Route('/movie', name: 'movie')]
    public function index(Request $request): Response
    {
        $movieForm = $this->createForm(MovieType::class);
        $movieForm->handleRequest($request);

        if ($movieForm->isSubmitted() && $movieForm->isValid()) {
            /** @var Movie $movie */
            $movie = $movieForm->getData();
            $entityManager = $this->getDoctrine()->getManager();

            $movie->setAddedBy($this->getUser());

            $entityManager->persist($movie);
            $entityManager->flush();
            // bin/console doctrine:query:sql "select * from movie"
            return new RedirectResponse('/');
        }

        return $this->render('movie/index.html.twig', [
            'movie_form' => $movieForm->createView(),
        ]);
    }
}
