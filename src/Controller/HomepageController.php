<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomepageController extends AbstractController
{
    #[Route('/hello/{name}', name: 'homepage', requirements: ['name' => '.+'])]
    public function index(string $name): Response
    {
        return $this->render('homepage/index.html.twig', [
            'name' => $name,
        ]);
    }
}
