<?php

namespace App\Controller;

use App\Entity\Movie;
use App\Gateway\OmdbGateway;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("view", subject="movie")
 */
class ShowMovieController extends AbstractController
{
    public function __construct(
        private OmdbGateway $omdbGateway,
    ) {
    }

    #[Route('/movie/{id}', name: 'show_movie')]
    public function index(Movie $movie): Response
    {
        $movieDirector = $this->omdbGateway
            ->getDirectorByMovie($movie);

        $response = $this->render('show_movie/index.html.twig', [
            'movie' => $movie,
            'movie_director' => $movieDirector,
        ]);

        return $response;
    }
}
