<?php

namespace App\Gateway;

use App\Entity\Movie;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class OmdbGateway
{
    public function __construct(
        private HttpClientInterface $httpClient,
    ) {
    }

    public function getDirectorByMovie(Movie $movie): string
    {
        $requestUrl = sprintf(
            'http://www.omdbapi.com/?apikey=%s&t=%s',
            'e08bb81f',
            $movie->getTitle(),
        );

        $apiResponse = $this->httpClient->request(
            'GET', $requestUrl
        )->toArray();

        return $apiResponse['Director'];
    }
}
