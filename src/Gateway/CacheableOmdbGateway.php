<?php

namespace App\Gateway;

use App\Entity\Movie;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;

class CacheableOmdbGateway extends OmdbGateway
{
    public function __construct(
        private OmdbGateway $actualGateway,
        private CacheInterface $cache,
    ) {
    }

    public function getDirectorByMovie(Movie $movie): string
    {
        return $this->cache->get(
            'omdb_director_'.$movie->getHash(),
            function (ItemInterface $item) use ($movie) {
                $item->expiresAfter(10);

                return $this->actualGateway->getDirectorByMovie($movie);
            }
        );
    }
}
