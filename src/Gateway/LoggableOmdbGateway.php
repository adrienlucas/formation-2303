<?php

namespace App\Gateway;

use App\Entity\Movie;
use Psr\Log\LoggerInterface;

class LoggableOmdbGateway extends OmdbGateway
{
    public function __construct(
        private LoggerInterface $logger,
        private OmdbGateway $actualGateway,
    ) {
    }

    public function getDirectorByMovie(Movie $movie): string
    {
        $this->logger->info('OMDB was requested');

        return $this->actualGateway->getDirectorByMovie($movie);
    }
}
