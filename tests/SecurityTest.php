<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SecurityTest extends WebTestCase
{
    public function testItDeniesAccessWhenNotAuthenticated(): void
    {
        $client = static::createClient();
        $client->request('GET', '/movie');

        $this->assertResponseRedirects();
        $this->assertStringEndsWith(
            '/login',
            $client->getResponse()->headers->get('Location')
        );
    }

    public function testItDeniesAccessWhenWronglyAuthenticated(): void
    {
        $client = static::createClient();

        $client->request('GET', '/login');
        $client->submitForm(
            button: 'Login',
            fieldValues: [
                '_username' => 'adrien',
                '_password' => 'bad_pass',
            ]
        );

        $this->assertResponseRedirects();
        $this->assertStringEndsWith(
            '/login',
            $client->getResponse()->headers->get('Location')
        );
        $client->followRedirect();

        $this->assertSelectorTextContains('body', 'Bad username/password couple.');
    }

    public function testItDeniesAccessWhenNotAdmin(): void
    {
        $client = static::createClient();
        $client->request('GET', '/login');
        $client->submitForm(
            button: 'Login',
            fieldValues: [
                '_username' => 'erwan',
                '_password' => 'soleil',
            ]
        );

        $client->request('GET', '/movie');

        $this->assertSame(
            403,
            $client->getResponse()->getStatusCode()
        );
    }

    public function testItGrantsAccessWhenAuthenticatedAsAdmin(): void
    {
        $client = static::createClient();
        $client->request('GET', '/login');
        $client->submitForm(
            button: 'Login',
            fieldValues: [
                '_username' => 'adrien',
                '_password' => 'toto123',
            ]
        );
        $client->request('GET', '/movie');

        $this->assertSame(
            200,
            $client->getResponse()->getStatusCode()
        );
    }
}
