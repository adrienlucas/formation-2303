<?php

namespace App\Tests;

use App\Entity\Movie;
use Doctrine\Bundle\DoctrineBundle\DataCollector\DoctrineDataCollector;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class MovieTest extends WebTestCase
{
    public function testItCanCreateAMovie(): void
    {
        $this->markTestSkipped('not ready'); //@todo : make it pass
        $client = static::createClient();

        $client->request('GET', '/movie');

//        $this->assertTrue($client->getResponse()->isSuccessful());
//        $h2 = $crawler->filter('h2');
//        $this->assertCount(1, $h2);
//        $this->assertStringContainsString(
//            'Création de film',
//            $h2->text()
//        );
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h2', 'Création de film');

        $form = $client->getCrawler()->filter(
            '.container form[name="movie"]'
        )->form();

        $expectedTitle = 'Alice in wonderland';
        $client->enableProfiler();
        $client->submit($form, [
            'movie[title]' => $expectedTitle,
            'movie[releaseDate]' => '2010-2-25',
        ]);

        /** @var DoctrineDataCollector $doctrineCollector */
        $doctrineCollector = $client->getProfile()
            ->getCollector('db');

        $queries = $doctrineCollector->getQueries();
        $filteredQueries = array_filter(
            $queries['default'],
            fn ($query) => strpos($query['sql'], 'INSERT INTO movie') === 0
        );
        $this->assertCount(1, $filteredQueries);

        // Verify the movie is in database
//        $doctrine = self::$container->get('doctrine');
//        $movieRepository = $doctrine->getRepository(Movie::class);
//        $movies = $movieRepository->findByTitle($expectedTitle);
//        $this->assertCount(1, $movies);
    }
}
