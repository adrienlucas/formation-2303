

test:
	rm -f var/data_test.db
	bin/console doctrine:database:create --env=test
	bin/console doctrine:schema:update --force --env=test
	bin/phpunit
